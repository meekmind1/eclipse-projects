package firstSeleniumPackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class seleniumBasicForDemo {
	
	
WebDriver driver;
	
	public void browserInvoke() {
		try {
			System.setProperty("webdriver.chrome.driver","/home/meekmind/Documents/Selenium/chromedriver_linux64/chromedriver");
			driver= new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
			getCommands();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void getCommands() {
		try {
			driver.get("http:\\www.google.com/");
			String title =driver.getTitle();
			System.out.println("The title of the page is "+ title);
			String url=driver.getCurrentUrl();
			System.out.println("The current URL is "+url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		

	}
	
	public static void main(String[] args) {
		seleniumBasicForDemo myObj= new seleniumBasicForDemo();
		myObj.browserInvoke();
	}

}
