package javaBasic;

import java.util.Scanner;
public class whileLoopAvg {

	public static void main(String[] args) {
		System.out.println("Enter 10 numbers:");
		Scanner input = new Scanner(System.in);
		int total=0;
		int counter=0;
		int grade;
		int average;
		while(counter<10) {
			grade=input.nextInt();
			total=total+grade;
			counter++;
		}
		average=total/counter;
		System.out.println("Average is "+average);
		System.out.println("Counter is "+ counter);
	}

}


