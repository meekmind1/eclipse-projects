package javaBasic;

public class multiDimentionalArray {
	public static void main(String[] args) {
		int firstArray[][]= {{1, 2, 3, 4, 5, 6}, {6, 5, 4, 3, 2, 1}};
		int secondArray[][]= {{9, 8, 7, 6, 5, 4,}, {4, 5, 6, 7, 8, 9}};

		System.out.println("This is the first Array");
		display(firstArray);
		
		System.out.println("This is the Second Array");
		display(secondArray);
	}
	public static void display(int x[][]) {
		for (int row=0;row< x.length;row++) {
			for(int column=0; column<x[row].length;column++) {
				System.out.print(x[row][column]+"\t");
			}
			System.out.println();
	}
	}
}
